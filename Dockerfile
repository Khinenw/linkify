FROM node:15-alpine as builder
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
COPY . .

RUN npm run build

FROM nginx:1.19
COPY --from=builder /usr/src/app/build /usr/share/nginx/linkify
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf

